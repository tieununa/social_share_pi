var express = require("express");
var path = require("path");
var favicon = require("serve-favicon");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var morgan = require("morgan");

var logger = require("./common/logger");
var gameSettings = require("./helpers/gameSettings");

var index = require("./routes/index");
var shareResult = require("./routes/shareResult");
var facebookLink = require("./routes/facebookLink");
var labs = require("./routes/labs");

var app = express();

function initialize() {
    return logger.init(path.join(__dirname, "..", "logs", "socialshare")).then(() => gameSettings.load(path.join(__dirname, "settings.json")));
}

initialize()
    .then(() => {
        logger.info("Social Share Server is running...");

        // view engine setup
        app.set("views", path.join(__dirname, "views"));
        app.set("view engine", "hbs");

        // uncomment after placing your favicon in /public
        //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
        app.use(morgan("dev", { stream: logger.stream }));
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(cookieParser());
        app.use(express.static(path.join(__dirname, "public")));

        app.use("/", index);
        app.use("/shareResult", shareResult);
        app.use("/facebookLink", facebookLink);
        app.use("/labs", labs);

        // catch 404 and forward to error handler
        app.use(function(req, res, next) {
            var err = new Error("Not Found: " + req.path);
            err.status = 404;
            next(err);
        });

        // error handler
        app.use(function(err, req, res, next) {
            // set locals, only providing error in development
            res.locals.message = err.message;
            res.locals.error = req.app.get("env") === "development" ? err : {};

            // render the error page
            res.status(err.status || 500);
            res.render("error");

            console.error(err);
        });

        app.use("/games", function(req, res, next) {
            res.sendFile(path.join(__dirname + '/index.html'));
        });

        app.listen(8123);

        //console.logs("AAAAA:"+app.address().port);
    })
    .catch(console.error);
module.exports = app;
