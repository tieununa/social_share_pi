var fs = require("fs");

var logger = require("../common/logger");

var _gameSettings = {};

function load(filename) {
    return new Promise((resolve, reject) => {
        logger.info("Load settings file: " + filename);

        fs.readFile(filename, "utf8", function (err, text) {
            if (err) {
                reject(err);
            } else {
                try {
                    _gameSettings = JSON.parse(text);    
                    resolve(_gameSettings);
                } catch (ex) {
                    reject(ex);
                }
            }
        });
    });
}

function get(gameName, platform) {
    return _gameSettings[gameName];
}

module.exports = {
    load,
    get
};
