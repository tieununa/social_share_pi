var temp = require("temp");

var S3_OPTIONS = {
    maxAsyncS3: 20, // this is the default
    s3RetryCount: 3, // this is the default
    s3RetryDelay: 1000, // this is the default
    multipartUploadThreshold: 20971520, // this is the default (20 MB)
    multipartUploadSize: 15728640, // this is the default (15 MB)
    s3Options: {
        accessKeyId: "AKIA5YELWPAIMYCLRITI",
        secretAccessKey: "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"
    }
};

var MAX_CONCURRENT_UPLOAD_TO_S3 = 40;
var MAX_CONCURRENT_GENERATE_IMAGE = 10;

var TEMP_IMAGE_FOLDER;

function getTempImageFolder() {    
    if (TEMP_IMAGE_FOLDER) {
        return Promise.resolve(TEMP_IMAGE_FOLDER);
    }

    return new Promise((resolve, reject) => {
        temp.mkdir("socialshare", (err, dirPath) => {
            if (err) {
                reject(err);
            } else {
                TEMP_IMAGE_FOLDER = dirPath;
                resolve(dirPath);
            }
        });
    });    
}

module.exports = {
    S3_OPTIONS,
    MAX_CONCURRENT_UPLOAD_TO_S3,
    MAX_CONCURRENT_GENERATE_IMAGE,

    getTempImageFolder
};
