#/bin/bash
export REMOTE=34.234.168.136
export REMOTE_PATH=/home/ec2-user/Parse/real/parse_server/socialshare
export PEM_FILE=~/Documents/ama_test_server.pem

rsync -e "ssh -i $PEM_FILE" -ar --progress --exclude node_modules . ec2-user@$REMOTE:$REMOTE_PATH
ssh -i $PEM_FILE ec2-user@$REMOTE pm2 start $REMOTE_PATH/pm2.json
ssh -i $PEM_FILE ec2-user@$REMOTE sudo cp $REMOTE_PATH/nginx_socialshare.conf /etc/nginx/conf.d
ssh -i $PEM_FILE ec2-user@$REMOTE sudo service nginx restart
