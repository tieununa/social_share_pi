const fse = require("fs-extra");
const path = require("path");

const winston = require("winston");
const winstonDailyRotateFile = require("winston-daily-rotate-file");
winston.emitErrs = true;

const logger = new winston.Logger({
    transports: [
        new winston.transports.Console({
            level: "debug",
            handleExceptions: true,
            json: false,
            colorize: true
        })
    ],
    exitOnError: false
});

module.exports = logger;

module.exports.stream = {
    write: function(message, encoding) {
        logger.info(message);
    }
};

module.exports.init = dir => {
    return new Promise((resolve, reject) => {
        fse.mkdirp(dir, function(err) {
            if (err) {
                console.error(err);
            }

            logger.add(winstonDailyRotateFile, {
                level: "debug",
                filename: path.join(dir, "-all-logs.log"),
                handleExceptions: true,
                json: true,
                maxsize: 5242880,
                maxFiles: 5,
                colorize: false,
                datePattern: "yyyy-MM-dd",
                prepend: true
            });

            resolve();
        });
    });
};
