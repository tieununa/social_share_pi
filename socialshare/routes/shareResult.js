var express = require("express");
var router = express.Router();

var path = require("path");
var querystring = require("querystring");
var shortid = require("shortid");

var logger = require("../common/logger");
var deviceAgent = require("../common/deviceAgent");
var gameSettings = require("../helpers/gameSettings");

var workerGenerateImage = require("../worker/workerGenerateImage");

router.post("/discard", function(req, res, next) {
    var image = req.body.image;

    // TODO
    // Remove the generated image

    res.json({});
});

router.post("/", function(req, res, next) {
    // Find the valid settings
    var device = deviceAgent(req.get("User-Agent"), { parseUserAgent: true });

    var platform = "ios";
    if (device.model.toLowerCase() === "iphone" || device.model.toLowerCase() === "ipad" || device.os.family.toLowerCase() === "ios") {
        platform = "ios";
    } else if (device.model.toLowerCase() === "android" || device.os.family.toLowerCase() === "android") {
        platform = "android";
    }

    // Parse user data
    var dataString = req.body.data;

    var data;
    try {
        data = JSON.parse(dataString);
    } catch (ex) {
        return next(ex);
    }

    var { appId, typeScene, fbToken, songName, totalFriends, rawDataUser } = data;

    var settings = gameSettings.get(appId, platform);
    if (!settings) {
        return next(new Error("Invalid app id"));
    }

    var myScore = data.myScore || 0;
    var myRank = data.myRank || 0;
    var lbRank = data.myRank || 0;
    if (rawDataUser) {
        rawDataUser.forEach(userData => {
            var { rank, fbId, name, totalCrown, level, score, me } = userData;

            if (me) {
                myRank = rank;
                myScore = score;
            }
        });
    }

    var defeatCount = Math.abs(totalFriends - myRank);

    // Generate content
    var shareId = shortid.generate();
    var today = new Date();
    var contentPath = today.getFullYear() + "/" + (today.getMonth() + 1);

    var content = settings.content[typeScene] || settings.content.default;

    var shareLinkData = JSON.stringify({
        si: shareId,
        cp: contentPath,
        ts: typeScene,
        sn: songName,
        tf: totalFriends,
        mr: myRank,
        lbr: lbRank
    });

    var shareLink = settings.shareLink + "?b=" + new Buffer(shareLinkData).toString("base64");
    var imageLink = settings.imageStorageLink + "/" + contentPath + "/" + shareId + ".jpg";
    workerGenerateImage.enqueueJob(
        Object.assign(
            {
                shareId,
                contentPath,
                myScore,
                myRank,
                defeatCount,
                shareLink,
                bucketName: settings.s3.bucketName
            },
            data
        )
    );

    res.json({
        image: imageLink,
        link: shareLink,
        title: content.title,
        description: content.description
            .replace(/{{songname}}/g, songName)
            .replace(/{{defeatCount}}/g, defeatCount)
            .replace(/{{rank}}/, myRank)
            .replace(/{{lbrank}}/, lbRank)
    });
});

module.exports = router;
