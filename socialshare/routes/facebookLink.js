var express = require("express");
var router = express.Router();

var querystring = require("querystring");

var deviceAgent = require("../common/deviceAgent");
var gameSettings = require("../helpers/gameSettings");

router.get("/:gameName", function(req, res, next) {
    var gameName = req.params["gameName"];
    var shareLinkData = {};
    try {
        var blob = new Buffer(req.query.b, "base64").toString("utf8");
        shareLinkData = JSON.parse(blob);
    } catch (ex) {}

    var { si, cp, ts, sn, tf, mr, lbr } = shareLinkData;
    var shareId = si;
    var contentPath = cp;
    var typeScene = ts;
    var songName = sn;
    var totalFriends = tf;
    var myRank = mr;
    var lbRank = lbr;

    var defeatCount = Math.abs(totalFriends - myRank);

    var device = deviceAgent(req.get("User-Agent"), { parseUserAgent: true });

    var platform = "ios";
    if (device.model.toLowerCase() === "iphone" || device.model.toLowerCase() === "ipad" || device.os.family.toLowerCase() === "ios") {
        platform = "ios";
    } else if (device.model.toLowerCase() === "android" || device.os.family.toLowerCase() === "android") {
        platform = "android";
    }

    var settings = gameSettings.get(gameName, platform);
    var content = settings.content[typeScene];

    var description = content.description
        .replace(/{{songname}}/g, songName)
        .replace(/{{defeatCount}}/g, defeatCount)
        .replace(/{{rank}}/, myRank)
        .replace(/{{lbrank}}/, lbRank);
    var shareLink = settings.shareLink + "?" + querystring.stringify(req.query);
    var imageUrl = settings.imageStorageLink + "/" + contentPath + "/" + shareId + ".jpg";

    res.render("sharePage", {
        description,
        shareLink,
        imageUrl,
        androidAppUrl: settings.androidAppUrl,
        androidPackageName: settings.androidPackageName,
        androidAppName: settings.androidAppName,
        iosAppUrl: settings.iosAppUrl,
        iosStoreId: settings.iosStoreId,
        gameTitle: settings.gameTitle,
        fbAppId: settings.fbAppId,
        browserRedirectLink: settings.browserRedirectLink,
        iosRedirectLink: content.iosLink,
        androidMarketLink: content.androidLink,
        androidPlayStoreLink: content.androidLink,
        layout: false
    });
});

module.exports = router;
