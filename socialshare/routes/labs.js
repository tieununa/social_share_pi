var express = require("express");
var router = express.Router();

var path = require("path");
var fse = require("fs-extra");

var shortid = require("shortid");
var sharp = require("sharp");
var temp = require("temp");
var fetch = require("node-fetch");

var config = require("../config");
var logger = require("../common/logger");

var ImageBuilder = require("../worker/imageBuilder");
var workerUploadFileToS3 = require("../worker/workerUploadFileToS3");

/* GET home page. */
router.get("/", function(req, res, next) {
    res.render("labs", {
        code: "",
        imageUrl: "",
        userdata: "",
        layout: false
    });
});

router.post("/draw", function(req, res, next) {
    var code = req.body.code;
    var imageUrl = "";

    var userdata = {};
    try {
        userdata = JSON.parse(req.body.userdata);
    } catch (ex) {    
    }

    eval(code);

    var rawDataUser = userdata.rawDataUser;
    var totalFriends = userdata.totalFriends;

    var myScore = 0;
    var myRank = 0;
    if (rawDataUser) {
        rawDataUser.forEach(userData => {
            var { rank, fbId, name, totalCrown, level, score, me } = userData;

            if (me) {
                myRank = rank;
                myScore = score;
            }
        });
    }

    var defeatCount = Math.abs(totalFriends - myRank);

    testDraw(Object.assign({
        myScore,
        myRank,
        appId: "pianoidol",
        typeScene: "normalResultScore",
        songName: "Canon In D",
        rawDataUser: [
            {
                me: true,
                score: 999
            }
        ],
        shareId: shortid.generate(),
    }, userdata))
        .then(tempImagePath => {
            return workerUploadFileToS3
                .uploadToS3(tempImagePath, "pianoidol", "socialshare/labs")
                .then(() => {
                    fse.remove(tempImagePath);

                    imageUrl = "https://s3-ap-southeast-1.amazonaws.com/pianoidol/socialshare/labs/" + path.basename(tempImagePath);

                    res.render("labs", {
                        code,
                        imageUrl,
                        userdata: JSON.stringify(userdata),
                        layout: false
                    });
                })
                .catch(next);
        })
        .catch(next);
});


router.post("/games", function(req, res, next) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

module.exports = router;
