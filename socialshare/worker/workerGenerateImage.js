var path = require("path");
var querystring = require("querystring");
var fse = require("fs-extra");
var fetch = require("node-fetch");

var config = require("../config");
var logger = require("../common/logger");

var ImageBuilder = require("./imageBuilder");
var workerUploadFileToS3 = require("./workerUploadFileToS3");

var _generateImageTasks = [];

function runGenerateImageWorker() {
    if (_generateImageTasks.length === 0) {
        return setTimeout(() => runGenerateImageWorker(), 1000);
    }

    var works = [];
    for (var i = 0; i < config.MAX_CONCURRENT_GENERATE_IMAGE; ++i) {
        var task = _generateImageTasks.shift();
        if (!task) {
            break;
        }

        (function(task) {
            works.push(processGenerateImageTask(task));
        })(task);
    }

    if (works.length > 0) {
        Promise.all(works)
            .then(() => {
                setTimeout(() => runGenerateImageWorker(), 1000);
            })
            .catch(err => {
                logger.error(err);
                setTimeout(() => runGenerateImageWorker(), 1000);
            });
    } else {
        setTimeout(() => runGenerateImageWorker(), 1000);
    }
}

function uploadDefaultImage(task) {
    var { appId, typeScene, songName, myScore, shareId, bucketName, contentPath } = task;
    var targetFileName = shareId + ".jpg";

    return workerUploadFileToS3.enqueueJob({
        targetFileName,
        localFile: path.join(__dirname, "..", "data", appId, "default.jpg"),
        bucketName: bucketName,
        contentPath: "socialshare/" + contentPath,
        preserveFile: true
    });
}

function forceFacebookScrape(task) {
    var { shareLink } = task;
    var ogLink = "https://graph.facebook.com/?id=" + querystring.escape(shareLink) + "&scrape=true";
    return fetch(ogLink);
}

function processGenerateImageTask(task) {
    return new Promise((resolve, reject) => {
        var { appId, typeScene, songName, myScore, shareId, bucketName, contentPath } = task;
        var targetFileName = shareId + ".jpg";

        var imageCreator = createImageFunction[appId][typeScene];
        if (imageCreator) {
            try {
                imageCreator(task)
                    .then(filePath => {
                        return workerUploadFileToS3
                            .enqueueJob({
                                targetFileName,
                                localFile: filePath,
                                bucketName: bucketName,
                                contentPath: "socialshare/" + contentPath
                            })
                            .then(() => {
                                forceFacebookScrape(task);
                                resolve();
                            })
                            .catch(reject);
                    })
                    .catch(err => {
                        uploadDefaultImage(task)
                            .then(() => {
                                forceFacebookScrape(task);
                                resolve();
                            })
                            .catch(reject);
                    });
            } catch (ex) {
                logger.error(ex);

                uploadDefaultImage(task)
                    .then(() => {
                        forceFacebookScrape(task);
                        resolve();
                    })
                    .catch(reject);
            }
        } else {
            logger.error(new Error("No image builder found: [" + appId + "] [" + typeScene + "]"));

            uploadDefaultImage(task)
                .then(() => {
                    forceFacebookScrape(task);
                    resolve();
                })
                .catch(reject);
        }
    });
}

/** 
 * Queue a task for generate image worker
 * @method
 * @param {object} task - the task object
 * @returns {Promise} Promise represents the completion
 */
function enqueueJob(task) {
    _generateImageTasks.push(task);
    return Promise.resolve(task);
}

/** 
 * Check if image builder is available for the task
 * @method
 * @param {object} task - the task object
 * @return {boolean} the result
 */
var isImageBuilderAvailable = function(task) {
    var { appId, typeScene } = task;
    var imageCreator = createImageFunction[appId][typeScene];
    return imageCreator != null;
};

var createImageFunction = {
    pianoidol: {
        normalResultScore: function(task) {
            var { appId, typeScene, songName, shareId, rawDataUser, totalFriends, myScore, myRank } = task;
            var imageBuilder = new ImageBuilder(task);

            imageBuilder.loadBackground("result-bg.png");
            imageBuilder.drawText(songName, 50, 75, 1100, 70, "middle", "white", 62, { fontFamily: "Open Sans", fontWeight: "bold" });
            imageBuilder.drawText(myScore.toString(), 50, 205, 1100, 140, "middle", "white", 130, { fontFamily: "Open Sans", fontWeight: "bold" });

            return imageBuilder.finalize();
        },
        challengeResult: function(task) {
            var { appId, typeScene, songName, shareId, rawDataUser, totalFriends, myScore, myRank } = task;
            var imageBuilder = new ImageBuilder(task);

            imageBuilder.loadBackground("result-bg.png");
            imageBuilder.drawText(songName, 50, 75, 1100, 70, "middle", "white", 62, { fontFamily: "Open Sans", fontWeight: "bold" });
            imageBuilder.drawText(myScore.toString(), 50, 205, 1100, 140, "middle", "white", 130, { fontFamily: "Open Sans", fontWeight: "bold" });

            return imageBuilder.finalize();
        },
        multiplayerResult: function(task) {
            var { appId, typeScene, songName, shareId, rawDataUser, totalFriends, myScore, myRank } = task;
            var imageBuilder = new ImageBuilder(task);

            imageBuilder.loadBackground("online-bg.png");

            function drawResultBox(index) {
                //draw name
                imageBuilder.drawText(
                    rawDataUser[index].name.length >= 14 ? rawDataUser[index].name.substr(0, 12) + "..." : rawDataUser[index].name,
                    index == 0 || index == 2 ? 157 : 746,
                    index < 2 ? 212 : 375,
                    400,
                    28,
                    "left",
                    "white",
                    26,
                    { fontFamily: "Open Sans" }
                );

                //draw level
                imageBuilder.drawText(
                    rawDataUser[index].score > 0 ? rawDataUser[index].score : "...",
                    index == 0 || index == 2 ? 157 : 746,
                    index < 2 ? 250 : 413,
                    200,
                    28,
                    "left",
                    "white",
                    26,
                    { fontFamily: "Open Sans" }
                );

                //draw Avatar
                if (rawDataUser[index].fbId) {
                    imageBuilder.drawFacebookAvatar(rawDataUser[index].fbId, index == 0 || index == 2 ? 40 : 631, index < 2 ? 215 : 379, 100, 100);
                }
            }
            drawResultBox(0);
            drawResultBox(1);
            drawResultBox(2);
            drawResultBox(3);
            return imageBuilder.finalize();
        },
        rankUp: function(task) {
            var { appId, typeScene, songName, shareId, rawDataUser, totalFriends, myScore, myRank } = task;
            var imageBuilder = new ImageBuilder(task);

            imageBuilder.loadBackground("rank_up.png");
            function drawResultBox(index) {
                //draw name
                imageBuilder.drawText(
                    rawDataUser[index].name.length >= 14 ? rawDataUser[index].name.substr(0, 12) + "..." : rawDataUser[index].name,
                    index != 0 ? 645 : 175,
                    380,
                    200,
                    60,
                    "middle",
                    "white",
                    35,
                    { fontFamily: "Open Sans" }
                );

                //draw crown
                imageBuilder.drawText(rawDataUser[index].totalCrown.toString(), index != 0 ? 948 : 500, 295, 200, 40, "left", "white", 37, {
                    fontFamily: "Open Sans"
                });
                //draw level
                imageBuilder.drawText(rawDataUser[index].level, index != 0 ? 948 : 500, 372, 200, 40, "left", "white", 37, { fontFamily: "Open Sans" });
                //draw rank
                imageBuilder.drawText(
                    rawDataUser[index].rank,
                    index != 0 ? 1105 : 65,
                    324,
                    83,
                    57,
                    "left",
                    index == 0 ? "#21d95e" : "#991900",
                    rawDataUser[index].rank < 1000 ? 55 : 40,
                    { fontFamily: "Open Sans" }
                );

                //draw Avatar
                if (rawDataUser[index].fbId) {
                    imageBuilder.drawFacebookAvatar(
                        rawDataUser[index].fbId,
                        index != 0 ? 690 : 220,
                        index != 0 ? 290 : 284,
                        index != 0 ? 80 : 86,
                        index != 0 ? 80 : 86
                    );
                }
            }
            drawResultBox(0);
            drawResultBox(1);
            return imageBuilder.finalize();
        },
        friendLeaderboard: function(task) {
            var { appId, typeScene, songName, shareId, rawDataUser, totalFriends, myScore, myRank, indexMe } = task;
            var imageBuilder = new ImageBuilder(task);

            imageBuilder.loadBackground("leaderboard_" + indexMe + ".png");
            if (rawDataUser) {
                rawDataUser.sort(sortData);
            }
            if (typeScene == "globalLeaderboard") {
                imageBuilder.drawFile("global.png", 90, 10);
            } else if (typeScene == "friendLeaderboard") {
                imageBuilder.drawFile("friend.png", 90, 10);
            }

            var subTop = 0;
            var subLeft = 0;
            //User0 l t w h
            function sortData(s1, s2) {
                return s1.rank > s2.rank ? 1 : s1.rank < s2.rank ? -1 : 0;
            }

            function drawResultBox(index) {
                //draw name
                imageBuilder.drawText(
                    rawDataUser[index].name.length >= 14 ? rawDataUser[index].name.substr(0, 12) + "..." : rawDataUser[index].name,
                    !rawDataUser[index].me ? 510 - subLeft : 480 - subLeft,
                    !rawDataUser[index].me ? 180 - subTop : 192 - subTop,
                    400,
                    50,
                    "left",
                    "white",
                    35,
                    { fontFamily: "Open Sans", fontWeight: "bold" }
                );

                //draw crown
                imageBuilder.drawText(
                    rawDataUser[index].totalCrown.toString(),
                    !rawDataUser[index].me ? 900 - subLeft : 967 - subLeft,
                    !rawDataUser[index].me ? 190 - subTop : 205 - subTop,
                    200,
                    70,
                    "left",
                    "white",
                    40,
                    { fontFamily: "Open Sans" }
                );

                //draw level
                imageBuilder.drawText(
                    "Lv " + rawDataUser[index].level,
                    !rawDataUser[index].me ? 510 - subLeft : 480 - subLeft,
                    !rawDataUser[index].me ? 223 - subTop : 235 - subTop,
                    400,
                    45,
                    "left",
                    "white",
                    32,
                    { fontFamily: "Open Sans" }
                );

                //draw rank
                imageBuilder.drawText(
                    rawDataUser[index].rank,
                    !rawDataUser[index].me ? 205 - subLeft : 110 - subLeft,
                    !rawDataUser[index].me ? 190 - subTop : 205 - subTop,
                    !rawDataUser[index].me ? 175 : 220,
                    60,
                    "middle",
                    "#6d7f99",
                    60,
                    { fontFamily: "Open Sans", fontWeight: "bold" }
                );

                //draw Avatar
                if (rawDataUser[index].fbId) {
                    imageBuilder.drawFacebookAvatar(
                        rawDataUser[index].fbId,
                        !rawDataUser[index].me ? 422 - subLeft : 377 - subLeft,
                        !rawDataUser[index].me ? 194 - subTop : 200 - subTop,
                        !rawDataUser[index].me ? 67 : 84,
                        !rawDataUser[index].me ? 67 : 84
                    );
                }
            }
            drawResultBox(0);

            subTop = -160;
            if (indexMe != 1) {
                subTop = -130;
            }
            subLeft = 0;
            drawResultBox(1);

            subTop = -290;
            if (indexMe == 3) {
                subTop = -260;
            }
            subLeft = 0;
            drawResultBox(2);

            return imageBuilder.finalize();
        },
        globalLeaderboard: function(task) {
            var { appId, typeScene, songName, shareId, rawDataUser, totalFriends, myScore, myRank, indexMe } = task;
            var imageBuilder = new ImageBuilder(task);

            imageBuilder.loadBackground("leaderboard_" + indexMe + ".png");
            if (rawDataUser) {
                rawDataUser.sort(sortData);
            }
            if (typeScene == "globalLeaderboard") {
                imageBuilder.drawFile("global.png", 90, 10);
            } else if (typeScene == "friendLeaderboard") {
                imageBuilder.drawFile("friend.png", 90, 10);
            }

            var subTop = 0;
            var subLeft = 0;
            //User0 l t w h
            function sortData(s1, s2) {
                return s1.rank > s2.rank ? 1 : s1.rank < s2.rank ? -1 : 0;
            }

            function drawResultBox(index) {
                //draw name
                imageBuilder.drawText(
                    rawDataUser[index].name.length >= 14 ? rawDataUser[index].name.substr(0, 12) + "..." : rawDataUser[index].name,
                    !rawDataUser[index].me ? 510 - subLeft : 480 - subLeft,
                    !rawDataUser[index].me ? 180 - subTop : 192 - subTop,
                    400,
                    50,
                    "left",
                    "white",
                    35,
                    { fontFamily: "Open Sans", fontWeight: "bold" }
                );

                //draw crown
                imageBuilder.drawText(
                    rawDataUser[index].totalCrown.toString(),
                    !rawDataUser[index].me ? 900 - subLeft : 967 - subLeft,
                    !rawDataUser[index].me ? 190 - subTop : 205 - subTop,
                    200,
                    70,
                    "left",
                    "white",
                    40,
                    { fontFamily: "Open Sans" }
                );

                //draw level
                imageBuilder.drawText(
                    "Lv " + rawDataUser[index].level,
                    !rawDataUser[index].me ? 510 - subLeft : 480 - subLeft,
                    !rawDataUser[index].me ? 223 - subTop : 235 - subTop,
                    400,
                    45,
                    "left",
                    "white",
                    32,
                    { fontFamily: "Open Sans" }
                );

                //draw rank
                imageBuilder.drawText(
                    rawDataUser[index].rank,
                    !rawDataUser[index].me ? 205 - subLeft : 110 - subLeft,
                    !rawDataUser[index].me ? 190 - subTop : 205 - subTop,
                    !rawDataUser[index].me ? 175 : 220,
                    60,
                    "middle",
                    "#6d7f99",
                    60,
                    { fontFamily: "Open Sans", fontWeight: "bold" }
                );

                //draw Avatar
                if (rawDataUser[index].fbId) {
                    imageBuilder.drawFacebookAvatar(
                        rawDataUser[index].fbId,
                        !rawDataUser[index].me ? 422 - subLeft : 377 - subLeft,
                        !rawDataUser[index].me ? 194 - subTop : 200 - subTop,
                        !rawDataUser[index].me ? 67 : 84,
                        !rawDataUser[index].me ? 67 : 84
                    );
                }
            }
            drawResultBox(0);

            subTop = -160;
            if (indexMe != 1) {
                subTop = -130;
            }
            subLeft = 0;
            drawResultBox(1);

            subTop = -290;
            if (indexMe == 3) {
                subTop = -260;
            }
            subLeft = 0;
            drawResultBox(2);

            return imageBuilder.finalize();
        },
        globalSongLeaderboard: function(task) {
            var { appId, typeScene, songName, shareId, rawDataUser, totalFriends, myScore, myRank, indexMe } = task;
            var imageBuilder = new ImageBuilder(task);
            imageBuilder.loadBackground("song" + indexMe + ".png");
            imageBuilder.drawText(songName, 50, 50, 1100, 70, "middle", "white", 62, { fontFamily: "Open Sans", fontWeight: "bold" });
            var subTop = 0;
            var subLeft = 0;
            if (rawDataUser) {
                rawDataUser.sort(sortData);
            }
            //User0 l t w h
            function sortData(s1, s2) {
                return s1.rank > s2.rank ? 1 : s1.rank < s2.rank ? -1 : 0;
            }
            function parseRank(ranking) {
                if (ranking >= 1000000) {
                    return Math.floor(parseInt(ranking) / 1000000) + "M";
                } else {
                    return ranking;
                }
            }

            function drawResultBox(index) {
                //draw name
                imageBuilder.drawText(
                    rawDataUser[index].name.length >= 14 ? rawDataUser[index].name.substr(0, 12) + "..." : rawDataUser[index].name,
                    !rawDataUser[index].me ? 510 - subLeft : 480 - subLeft,
                    !rawDataUser[index].me ? 180 - subTop + 15 : 192 - subTop + 18,
                    400,
                    50,
                    "left",
                    "white",
                    35,
                    { fontFamily: "Open Sans", fontWeight: "bold" }
                );

                //draw crown
                imageBuilder.drawText(
                    rawDataUser[index].score,
                    !rawDataUser[index].me ? 850 - subLeft : 917 - subLeft,
                    !rawDataUser[index].me ? 190 - subTop : 205 - subTop,
                    200,
                    70,
                    "left",
                    "white",
                    40,
                    { fontFamily: "Open Sans", fontWeight: "bold" }
                );

                //draw rank
                imageBuilder.drawText(
                    parseRank(rawDataUser[index].rank),
                    !rawDataUser[index].me ? 205 - subLeft : 110 - subLeft,
                    !rawDataUser[index].me ? 190 - subTop : 205 - subTop,
                    !rawDataUser[index].me ? 175 : 220,
                    60,
                    "middle",
                    "#6d7f99",
                    rawDataUser[index].rank.length < 4 ? 60 : 40,
                    { fontFamily: "Open Sans", fontWeight: "bold" }
                );

                //draw Avatar
                if (rawDataUser[index].fbId) {
                    imageBuilder.drawFacebookAvatar(
                        rawDataUser[index].fbId,
                        !rawDataUser[index].me ? 422 - subLeft : 377 - subLeft,
                        !rawDataUser[index].me ? 194 - subTop : 200 - subTop,
                        !rawDataUser[index].me ? 67 : 84,
                        !rawDataUser[index].me ? 67 : 84
                    );
                }
            }
            drawResultBox(0);

            subTop = -160;
            if (indexMe != 1) {
                subTop = -130;
            }
            subLeft = 0;
            drawResultBox(1);

            subTop = -290;
            if (indexMe == 3) {
                subTop = -260;
            }
            subLeft = 0;
            drawResultBox(2);

            return imageBuilder.finalize();
        },
        friendSongLeaderboard: function(task) {
            var { appId, typeScene, songName, shareId, rawDataUser, totalFriends, myScore, myRank, indexMe } = task;
            var imageBuilder = new ImageBuilder(task);
            imageBuilder.loadBackground("song" + indexMe + ".png");
            imageBuilder.drawText(songName, 50, 50, 1100, 70, "middle", "white", 62, { fontFamily: "Open Sans", fontWeight: "bold" });
            var subTop = 0;
            var subLeft = 0;
            if (rawDataUser) {
                rawDataUser.sort(sortData);
            }
            //User0 l t w h
            function sortData(s1, s2) {
                return s1.rank > s2.rank ? 1 : s1.rank < s2.rank ? -1 : 0;
            }
            function parseRank(ranking) {
                if (ranking >= 1000000) {
                    return Math.floor(parseInt(ranking) / 1000000) + "M";
                } else {
                    return ranking;
                }
            }

            function drawResultBox(index) {
                //draw name
                imageBuilder.drawText(
                    rawDataUser[index].name.length >= 14 ? rawDataUser[index].name.substr(0, 12) + "..." : rawDataUser[index].name,
                    !rawDataUser[index].me ? 510 - subLeft : 480 - subLeft,
                    !rawDataUser[index].me ? 180 - subTop + 15 : 192 - subTop + 18,
                    400,
                    50,
                    "left",
                    "white",
                    35,
                    { fontFamily: "Open Sans", fontWeight: "bold" }
                );

                //draw crown
                imageBuilder.drawText(
                    rawDataUser[index].score,
                    !rawDataUser[index].me ? 850 - subLeft : 917 - subLeft,
                    !rawDataUser[index].me ? 190 - subTop : 205 - subTop,
                    200,
                    70,
                    "left",
                    "white",
                    40,
                    { fontFamily: "Open Sans", fontWeight: "bold" }
                );

                //draw rank
                imageBuilder.drawText(
                    parseRank(rawDataUser[index].rank),
                    !rawDataUser[index].me ? 205 - subLeft : 110 - subLeft,
                    !rawDataUser[index].me ? 190 - subTop : 205 - subTop,
                    !rawDataUser[index].me ? 175 : 220,
                    60,
                    "middle",
                    "#6d7f99",
                    rawDataUser[index].rank.length < 4 ? 60 : 40,
                    { fontFamily: "Open Sans", fontWeight: "bold" }
                );

                //draw Avatar
                if (rawDataUser[index].fbId) {
                    imageBuilder.drawFacebookAvatar(
                        rawDataUser[index].fbId,
                        !rawDataUser[index].me ? 422 - subLeft : 377 - subLeft,
                        !rawDataUser[index].me ? 194 - subTop : 200 - subTop,
                        !rawDataUser[index].me ? 67 : 84,
                        !rawDataUser[index].me ? 67 : 84
                    );
                }
            }
            drawResultBox(0);

            subTop = -160;
            if (indexMe != 1) {
                subTop = -130;
            }
            subLeft = 0;
            drawResultBox(1);

            subTop = -290;
            if (indexMe == 3) {
                subTop = -260;
            }
            subLeft = 0;
            drawResultBox(2);

            return imageBuilder.finalize();
        }
    }
};

runGenerateImageWorker();

module.exports = {
    enqueueJob,
    isImageBuilderAvailable
};
