var util = require("util");
var temp = require("temp");
var fetch = require("node-fetch");

var path = require("path");
var sharp = require("sharp");

var config = require("../config");
var logger = require("../common/logger");

/**
 * Creates an image builder.
 * @class
 * @param {object} task - the task object to build image with
 */
var ImageBuilder = function(task) {
    this.rawImagePath = path.join(__dirname, "..", "data", task.appId);
    this.image = null;
    this.shareId = task.shareId;
    this.jobs = [];
};

module.exports = ImageBuilder;

/** 
 * Finalize the image creation
 * @method
 * @return {Promise} Promise represents the completion
 */
ImageBuilder.prototype.finalize = function() {
    return this.jobs
        .reduce(
            (prev, cur) =>
                prev.then(image => {
                    this.image = image;
                    return cur();
                }),
            Promise.resolve()
        )
        .then(image => {
            this.image = image;
            return saveImage(image, this.shareId);
        });
};

ImageBuilder.prototype.loadBackground = function(filename) {
    this.jobs.push(() => Promise.resolve(sharp(path.join(this.rawImagePath, filename))));
};

ImageBuilder.prototype.drawText = function(text, left, top, width, height, anchor, color, fontSize, opts) {
    this.jobs.push(() => {
        text = text || "";
        left = left || 0;
        top = top || 0;
        width = width || 0;
        height = height || 0;
        anchor = anchor || "middle";
        color = color || "white";

        if (!opts || typeof opts !== "object") {
            opts = {};
        }

        fontSize = fontSize || Math.floor(height / 2);

        var fontFamily = opts.fontFamily || "Open Sans";
        var fontWeight = opts.fontWeight || "normal";

        var innerX = 0;
        if (anchor === "middle") {
            innerX = Math.floor(width / 2);
        } else if (anchor === "right") {
            innerX = width;
        }

        var innerY = height - Math.floor((height - fontSize) / 2);

        var svgText = new Buffer(
            util.format(
                '<svg width="%d" height="%d"><text x="%d" y="%d" text-anchor="%s" font-family="%s" font-weight="%s" font-size="%d" fill="%s">%s</text></svg>',
                width,
                height,
                innerX,
                innerY,
                anchor,
                fontFamily,
                fontWeight,
                fontSize,
                color,
                text
            )
        );

        return overlayImageWith(this.image, svgText, { left, top });
    });
};

ImageBuilder.prototype.drawSVG = function(svgText, left, top) {
    this.jobs.push(() => {
        return overlayImageWith(this.image, new Buffer(svgText), { left, top });
    });
};

ImageBuilder.prototype.drawFile = function(filePath, left, top) {
    this.jobs.push(() => {
        return overlayImageWith(this.image, path.join(this.rawImagePath, filePath), { left, top });
    });
};

ImageBuilder.prototype.drawRemoteFile = function(fileUrl, left, top) {
    this.jobs.push(() => {
        return overlayImageWith(this.image, fileUrl, { left, top });
    });
};

ImageBuilder.prototype.drawFacebookAvatar = function(fbId, left, top, width, height) {
    this.jobs.push(() => {
        return overlayImageWith(this.image, "http://graph.facebook.com/" + fbId + "/picture?type=large&redirect=true&width=" + width + "&height=" + height, {
            left,
            top
        });
    });
};

/** 
 * Quick function to overlay a sharp image and return the mutated sharp image object
 * @method
 * @param {object} image - the source sharp image
 * @param {string|object} buffer - the SVG buffer or path string to a file, or url to the image in some remote server (ie. facebook)
 * @param {object} options - options of the composite operation
 * @returns {Promise} Promise represents the mutated sharp image object
 */
function overlayImageWith(image, bufferOrFile, options) {
    var buffer;
    var url;
    if (typeof bufferOrFile === "string") {
        url = bufferOrFile;
    } else {
        buffer = bufferOrFile;
    }

    if (url) {
        if (url.startsWith("http:") || url.startsWith("https:")) {
            return fetch(url)
                .then(res => res.buffer())
                .then(downloadedBuffer => {
                    if (!downloadedBuffer) {
                        return image;
                    }

                    var downloadedImage = sharp(downloadedBuffer);
                    return new Promise((resolve, reject) => {
                        downloadedImage.raw().toBuffer((err, rawBuffer, rawInfo) => {
                            if (err) {
                                return reject(err);
                            }

                            image
                                .overlayWith(
                                    rawBuffer,
                                    Object.assign(options, {
                                        raw: { width: rawInfo.width, height: rawInfo.height, channels: rawInfo.channels }
                                    })
                                )
                                .raw()
                                .toBuffer((err, data, resultInfo) => {
                                    if (err) {
                                        return reject(err);
                                    }
                                    resolve(sharp(data, { raw: { width: resultInfo.width, height: resultInfo.height, channels: resultInfo.channels } }));
                                });
                        });
                    });
                });
        } else {
            var overlayImage = sharp(url);
            return new Promise((resolve, reject) => {
                overlayImage.raw().toBuffer((err, rawBuffer, rawInfo) => {
                    if (err) {
                        return reject(err);
                    }

                    image
                        .overlayWith(
                            rawBuffer,
                            Object.assign(options, {
                                raw: { width: rawInfo.width, height: rawInfo.height, channels: rawInfo.channels }
                            })
                        )
                        .raw()
                        .toBuffer((err, data, resultInfo) => {
                            if (err) {
                                return reject(err);
                            }

                            resolve(sharp(data, { raw: { width: resultInfo.width, height: resultInfo.height, channels: resultInfo.channels } }));
                        });
                });
            });
        }
    }

    return new Promise((resolve, reject) => {
        image
            .overlayWith(buffer, options)
            .raw()
            .toBuffer((err, data, resultInfo) => {
                if (err) {
                    return reject(err);
                }

                resolve(sharp(data, { raw: { width: resultInfo.width, height: resultInfo.height, channels: resultInfo.channels } }));
            });
    });
}

/** 
 * Save an image to temp folder
 * @method
 * @param {object} image - the sharp object represents the image
 * @param {string} shareId - the sharing ID
 * @returns {Promise} Promise represents the completion
 */
function saveImage(image, shareId) {
    return new Promise((resolve, reject) => {
        config
            .getTempImageFolder()
            .then(dirPath => {
                var tempImagePath = path.join(dirPath, shareId + ".jpg");
                return image.jpeg({ progressive: true, quality: 80 }).toFile(tempImagePath, function(err, info) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(tempImagePath);
                    }
                });
            })
            .catch(reject);
    });
}
