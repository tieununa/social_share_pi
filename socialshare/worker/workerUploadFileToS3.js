var path = require("path");
var fse = require("fs-extra");

var async = require("async");
var s3 = require("s3");

var config = require("../config");
var logger = require("../common/logger");

var s3Client = s3.createClient(config.S3_OPTIONS);

var _uploadS3Tasks = [];

function runUploadS3Worker() {
    if (_uploadS3Tasks.length === 0) {
        return setTimeout(() => runUploadS3Worker(), 1000);   
    }    

    var works = [];
    for (var i = 0; i < config.MAX_CONCURRENT_UPLOAD_TO_S3; ++i) {
        var task = _uploadS3Tasks.shift();
        if (!task) {
            break;
        }

        (function(task) {
            works.push(processUploadS3(task));
        })(task);
    }

    if (works.length > 0) {
        Promise.all(works)
            .then(() => {
                setTimeout(() => runUploadS3Worker(), 1000);
            })
            .catch(err => {
                logger.error(err);
                setTimeout(() => runUploadS3Worker(), 1000);
            });
    } else {
        setTimeout(() => runUploadS3Worker(), 1000);
    }
}

function processUploadS3(task) {
    var { localFile, bucketName, contentPath, targetFileName, preserveFile } = task;    
    return uploadToS3(localFile, bucketName, contentPath, targetFileName, preserveFile);
}

/** 
 * Queue a task for upload file to s3 worker
 * @method
 * @param {object} task - the task object
 * @returns {Promise} Promise represents the completion
 */
function enqueueJob(task) {
    _uploadS3Tasks.push(task);
    return Promise.resolve(task);
}

function uploadToS3(filePath, bucketName, contentPath, targetFileName, preserveFile) {
    return new Promise((resolve, reject) => {        
        var params = {
            localFile: filePath,

            s3Params: {
                Bucket: bucketName,
                Key: contentPath + "/" + (targetFileName || path.basename(filePath)),
                ACL: "public-read"
                // other options supported by putObject, except Body and ContentLength.
                // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
            }
        };

        var uploader = s3Client.uploadFile(params);

        uploader.on("error", function(err) {
            reject(err);
        });

        uploader.on("progress", function() {
            //logger.info("progress", uploader.progressMd5Amount, uploader.progressAmount, uploader.progressTotal);
        });

        uploader.on("end", function() {            
            if (!preserveFile) {
                fse.remove(filePath);
            }
            resolve();
        });
    });
}

runUploadS3Worker();

module.exports = {
    enqueueJob,
    uploadToS3
};
